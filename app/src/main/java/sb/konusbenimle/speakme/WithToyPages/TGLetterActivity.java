package sb.konusbenimle.speakme.WithToyPages;

import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import sb.konusbenimle.speakme.VoiceExplanationPages.GSpeakActivity;

public class TGLetterActivity extends AppCompatActivity implements View.OnClickListener {

    private MediaPlayer mediaPlayer;
    String myUri, gemiUri, gozUri, gunesUri, supurgeUri, gazeteUri;
    Button bTGRecord, bTGUp, bGemiR, bGemiU, bGozR, bGozU, bGunesR, bGunesU, bSupurgeR, bSupurgeU, bGazeteR, bGazeteU;
    FirebaseAuth firebaseAuth;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    private String gFilepath, gemiFile, gozFile, gunesFile, supurgeFile, gazeteFile;
    private MediaRecorder recorder;
    private StorageReference rf, sr, storageReference, srw;
    private static final String LOG_TAG = "g";
    private AdView tg1, tg2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tgletter);

        bTGRecord = findViewById(R.id.tg_record);
        bTGRecord.setOnClickListener(this);
        bTGUp = findViewById(R.id.tg_up);
        bTGUp.setOnClickListener(this);

        bGemiR = findViewById(R.id.tgemi_record);
        bGemiR.setOnClickListener(this);
        bGemiU = findViewById(R.id.tgemi_up);
        bGemiU.setOnClickListener(this);

        bGozR = findViewById(R.id.tgoz_record);
        bGozR.setOnClickListener(this);
        bGozU = findViewById(R.id.tgoz_up);
        bGozU.setOnClickListener(this);

        bGunesR = findViewById(R.id.tgunes_record);
        bGunesR.setOnClickListener(this);
        bGunesU = findViewById(R.id.tgunes_up);
        bGunesU.setOnClickListener(this);

        bSupurgeR = findViewById(R.id.tsupurge_record);
        bSupurgeR.setOnClickListener(this);
        bSupurgeU = findViewById(R.id.tsupurge_up);
        bSupurgeU.setOnClickListener(this);

        bGazeteR = findViewById(R.id.tgazete_record);
        bGazeteR.setOnClickListener(this);
        bGazeteU = findViewById(R.id.tgazete_up);
        bGazeteU.setOnClickListener(this);

       /* FirebaseStorage storage=FirebaseStorage.getInstance();
        StorageReference storageRef=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/letters/g.mp4");
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                myUri=uri.toString();
            }
        });*/

        // get user
        firebaseAuth = FirebaseAuth.getInstance();
        rf = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/");
        sr = rf.child("User Letters").child("G Letter From User").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail()));
        srw = rf.child("User Words").child("G Words From User").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail()));

        gFilepath = Environment.getExternalStorageDirectory().getPath() + "/g.mp4";
        gemiFile = Environment.getExternalStorageDirectory().getPath() + "/gemi.mp4";
        gozFile = Environment.getExternalStorageDirectory().getPath() + "/goz.mp4";
        gunesFile = Environment.getExternalStorageDirectory().getPath() + "/gunes.mp4";
        supurgeFile = Environment.getExternalStorageDirectory().getPath() + "/supurge.mp4";
        gazeteFile = Environment.getExternalStorageDirectory().getPath() + "/gazate.mp4";

        tg1 = findViewById(R.id.tg1);

        AdRequest adRequest = new AdRequest.Builder().build();
        tg1.loadAd(adRequest);

        tg1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }
    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gFilepath);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void stopRecording() {
        if (recorder != null) {
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
        }
    }
    private void startPlaying() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gFilepath);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }


    private void startRecordingGemit() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gemiFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGemit() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gemiFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingGozt() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gozFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGozt() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gozFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }
    private void startRecordingGunest() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gunesFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGunest() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gunesFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }
    private void startRecordingSupurget() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(supurgeFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingSupurget() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(supurgeFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }
    private void startRecordingGazetet() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gazeteFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGazetet() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gazeteFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(View v) {
        if (v == bTGRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bTGRecord.getText().toString())) {
                startRecording();
                bTGRecord.setText(stop);
            } else {
                stopRecording();
                bTGRecord.setText(record);
                startPlaying();
            }
        } else if (v == bTGUp) {
            Uri uri = Uri.fromFile(new File(gFilepath));
            storageReference = sr.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGemiR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGemiR.getText().toString())) {
                startRecordingGemit();
                bGemiR.setText(stop);
            } else {
                stopRecording();
                bGemiR.setText(record);
                startPlayingGemit();
            }
        } else if (v == bGemiU) {
            Uri uri = Uri.fromFile(new File(gemiFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGozR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGozR.getText().toString())) {
                startRecordingGozt();
                bGozR.setText(stop);
            } else {
                stopRecording();
                bGozR.setText(record);
                startPlayingGozt();
            }
        } else if (v == bGozU) {
            Uri uri = Uri.fromFile(new File(gozFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGunesR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGunesR.getText().toString())) {
                startRecordingGunest();
                bGunesR.setText(stop);
            } else {
                stopRecording();
                bGunesR.setText(record);
                startPlayingGunest();
            }
        } else if (v == bGunesU) {
            Uri uri = Uri.fromFile(new File(gunesFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bSupurgeR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bSupurgeR.getText().toString())) {
                startRecordingSupurget();
                bSupurgeR.setText(stop);
            } else {
                stopRecording();
                bSupurgeR.setText(record);
                startPlayingSupurget();
            }
        } else if (v == bSupurgeU) {
            Uri uri = Uri.fromFile(new File(supurgeFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGazeteR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGazeteR.getText().toString())) {
                startRecordingGazetet();
                bGazeteR.setText(stop);
            } else {
                stopRecording();
                bGazeteR.setText(record);
                startPlayingGazetet();
            }
        } else if (v == bGazeteU) {
            Uri uri = Uri.fromFile(new File(gazeteFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        }
    }


    public void gemi(View view) {
    }

    public void goz(View view) {
    }

    public void gunes(View view) {
    }

    public void supurge(View view) {
    }

    public void gazete(View view) {
    }

    public void tghow(View view) {
        Intent intent = new Intent(getApplicationContext(), GSpeakActivity.class);
        startActivity(intent);
    }
}