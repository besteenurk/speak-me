package sb.konusbenimle.speakme;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import sb.konusbenimle.speakme.WithoutToyPages.SLetEx;
import sb.konusbenimle.speakme.WithoutToyPages.TLetEx;

public class MainPageNotRegActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView s_let, t_let, iPicArr;
    private AdView bf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page_not_reg);

        // iPicArr = findViewById(R.id.picArr);

        s_let = findViewById(R.id.s_let_ng);
        t_let = findViewById(R.id.t_let_ng);

        s_let.setOnClickListener(this);
        t_let.setOnClickListener(this);

        bf = findViewById(R.id.bf);

        AdRequest adRequest = new AdRequest.Builder().build();
        bf.loadAd(adRequest);

        bf.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
              //  Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == s_let) {
            Intent intent = new Intent(getApplicationContext(), SLetEx.class);
            startActivity(intent);
        } else if (v == t_let) {
            Intent intent = new Intent(getApplicationContext(), TLetEx.class);
            startActivity(intent);
        }
    }
}
