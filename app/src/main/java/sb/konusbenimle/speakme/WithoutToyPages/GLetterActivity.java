package sb.konusbenimle.speakme.WithoutToyPages;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import sb.konusbenimle.speakme.VoiceExplanationPages.GSpeakActivity;

public class GLetterActivity extends AppCompatActivity implements View.OnClickListener {

    Button bGHow, bGRecord, bGUp, bGemiR, bGemiU, bGozR, bGozU, bGunesR, bGunesU, bSupurgeR, bSupurgeU, bGazeteR, bGazeteU;
    private MediaPlayer mediaPlayer;
    String myUri, gemiUri, gozUri, gunesUri, supurgeUri, gazeteUri;
    private Uri file;
    FirebaseAuth firebaseAuth;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    private String gFilepath, gemiFile, gozFile, gunesFile, supurgeFile, gazeteFile;
    private MediaRecorder recorder;
    private StorageReference rf, sr, storageReference, srw;
    private static final String LOG_TAG = "g";
    private AdView g1, g2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gletter);

        bGHow = findViewById(R.id.g_how);
        bGHow.setOnClickListener(this);
        bGRecord = findViewById(R.id.g_record);
        bGRecord.setOnClickListener(this);
        bGUp = findViewById(R.id.g_up);
        bGUp.setOnClickListener(this);

        bGemiR = findViewById(R.id.gemi_record);
        bGemiR.setOnClickListener(this);
        bGemiU = findViewById(R.id.gemi_up);
        bGemiU.setOnClickListener(this);

        bGozR = findViewById(R.id.goz_record);
        bGozR.setOnClickListener(this);
        bGozU = findViewById(R.id.goz_up);
        bGozU.setOnClickListener(this);

        bGunesR = findViewById(R.id.gunes_record);
        bGunesR.setOnClickListener(this);
        bGunesU = findViewById(R.id.gunes_up);
        bGunesU.setOnClickListener(this);

        bSupurgeR = findViewById(R.id.supurge_record);
        bSupurgeR.setOnClickListener(this);
        bSupurgeU = findViewById(R.id.supurge_up);
        bSupurgeU.setOnClickListener(this);

        bGazeteR = findViewById(R.id.gazete_record);
        bGazeteR.setOnClickListener(this);
        bGazeteU = findViewById(R.id.gazete_up);
        bGazeteU.setOnClickListener(this);

        FirebaseStorage storage=FirebaseStorage.getInstance();
        StorageReference storageRef=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/letters/g.mp4");
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                myUri=uri.toString();
            }
        });
        StorageReference gemi=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/g/g1.mp4");
        gemi.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                gemiUri=uri.toString();
            }
        });
        StorageReference goz=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/g/g2.mp4");
        goz.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                gozUri=uri.toString();
            }
        });
        StorageReference gunes=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/g/g3.mp4");
        gunes.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                gunesUri=uri.toString();
            }
        });
        StorageReference supurge=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/g/g4.mp4");
        supurge.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                supurgeUri=uri.toString();
            }
        });
        StorageReference gazete=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/g/g5.mp4");
        gazete.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                gazeteUri=uri.toString();
            }
        });

        // get user
        firebaseAuth = FirebaseAuth.getInstance();
        rf = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/");
        sr = rf.child("User Letters").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail())).child("G Letter From User");
        srw = rf.child("User Words").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail())).child("G Words From User");

        gFilepath = Environment.getExternalStorageDirectory().getPath() + "/g.mp4";
        gemiFile = Environment.getExternalStorageDirectory().getPath() + "/gemi.mp4";
        gozFile = Environment.getExternalStorageDirectory().getPath() + "/goz.mp4";
        gunesFile = Environment.getExternalStorageDirectory().getPath() + "/gunes.mp4";
        supurgeFile = Environment.getExternalStorageDirectory().getPath() + "/supurge.mp4";
        gazeteFile = Environment.getExternalStorageDirectory().getPath() + "/gazate.mp4";

        g1 = findViewById(R.id.g1);

        AdRequest adRequest = new AdRequest.Builder().build();
        g1.loadAd(adRequest);

        g1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }
    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gFilepath);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void stopRecording() {
        if (recorder != null) {
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
        }
    }
    private void startPlaying() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gFilepath);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingGemi() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gemiFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGemi() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gemiFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingGoz() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gozFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGoz() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gozFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }
    private void startRecordingGunes() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gunesFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGunes() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gunesFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }
    private void startRecordingSupurge() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(supurgeFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingSupurge() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(supurgeFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }
    private void startRecordingGazete() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(gazeteFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingGazete() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(gazeteFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }


    @Override
    public void onClick(View v) {
        if (v == bGHow) {
            Intent intent = new Intent(getApplicationContext(), GSpeakActivity.class);
            startActivity(intent);
        } else if (v == bGRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGRecord.getText().toString())) {
                startRecording();
                bGRecord.setText(stop);
            } else {
                stopRecording();
                bGRecord.setText(record);
                startPlaying();
            }

        } else if (v == bGUp) {
            Uri uri = Uri.fromFile(new File(gFilepath));
            storageReference = sr.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGemiR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGemiR.getText().toString())) {
                startRecordingGemi();
                bGemiR.setText(stop);
            } else {
                stopRecording();
                bGemiR.setText(record);
                startPlayingGemi();
            }
        } else if (v == bGemiU) {
            Uri uri = Uri.fromFile(new File(gemiFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGozR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGozR.getText().toString())) {
                startRecordingGoz();
                bGozR.setText(stop);
            } else {
                stopRecording();
                bGozR.setText(record);
                startPlayingGoz();
            }
        } else if (v == bGozU) {
            Uri uri = Uri.fromFile(new File(gozFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGunesR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGunesR.getText().toString())) {
                startRecordingGunes();
                bGunesR.setText(stop);
            } else {
                stopRecording();
                bGunesR.setText(record);
                startPlayingGunes();
            }
        } else if (v == bGunesU) {
            Uri uri = Uri.fromFile(new File(gunesFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bSupurgeR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bSupurgeR.getText().toString())) {
                startRecordingSupurge();
                bSupurgeR.setText(stop);
            } else {
                stopRecording();
                bSupurgeR.setText(record);
                startPlayingSupurge();
            }
        } else if (v == bSupurgeU) {
            Uri uri = Uri.fromFile(new File(supurgeFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bGazeteR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bGazeteR.getText().toString())) {
                startRecordingGazete();
                bGazeteR.setText(stop);
            } else {
                stopRecording();
                bGazeteR.setText(record);
                startPlayingGazete();
            }
        } else if (v == bGazeteU) {
            Uri uri = Uri.fromFile(new File(gazeteFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        }
    }


    public void g_play(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(myUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void gemi(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(gemiUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void goz(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(gozUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void gunes(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(gunesUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void supurge(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(supurgeUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void gazete(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(gazeteUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}