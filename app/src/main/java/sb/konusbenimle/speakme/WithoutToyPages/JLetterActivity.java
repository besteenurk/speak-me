package sb.konusbenimle.speakme.WithoutToyPages;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import sb.konusbenimle.speakme.VoiceExplanationPages.JSpeakActivity;

public class JLetterActivity extends AppCompatActivity implements View.OnClickListener {

    Button bJHow, bJRecord, bJUp, bJipR, bJipU, bPijamaR, bPijamaU, bBagajR, bBagajU, bOjeR, bOjeU, bRujR, bRujU;
    private MediaPlayer mediaPlayer;
    String myUri, jipUri, pijamaUri, bagajUri, ojeUri, rujUri;
    private Uri file;
    FirebaseAuth firebaseAuth;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    private String jFilepath, jipFile, pijamaFile, bagajFile, ojeFile, rujFile;
    private MediaRecorder recorder;
    private StorageReference rf, sr, storageReference, srw;
    private static final String LOG_TAG = "j";
    private AdView j1, j2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jletter);

        bJHow = findViewById(R.id.j_how);
        bJHow.setOnClickListener(this);
        bJRecord = findViewById(R.id.j_record);
        bJRecord.setOnClickListener(this);
        bJUp = findViewById(R.id.j_up);
        bJUp.setOnClickListener(this);

        bJipR = findViewById(R.id.jip_record);
        bJipR.setOnClickListener(this);
        bJipU = findViewById(R.id.jip_up);
        bJipU.setOnClickListener(this);

        bPijamaR = findViewById(R.id.pijama_record);
        bPijamaR.setOnClickListener(this);
        bPijamaU = findViewById(R.id.pijama_up);
        bPijamaU.setOnClickListener(this);

        bBagajR = findViewById(R.id.bagaj_record);
        bBagajR.setOnClickListener(this);
        bBagajU = findViewById(R.id.bagaj_up);
        bBagajU.setOnClickListener(this);

        bOjeR = findViewById(R.id.oje_record);
        bOjeR.setOnClickListener(this);
        bOjeU = findViewById(R.id.oje_up);
        bOjeU.setOnClickListener(this);

        bRujR = findViewById(R.id.ruj_record);
        bRujR.setOnClickListener(this);
        bRujU = findViewById(R.id.ruj_up);
        bRujU.setOnClickListener(this);

        FirebaseStorage storage=FirebaseStorage.getInstance();
        StorageReference storageRef=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/letters/j.mp4");
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                myUri=uri.toString();
            }
        });
        StorageReference jip=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/j/j2.mp4");
        jip.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                jipUri=uri.toString();
            }
        });
        StorageReference pijama=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/j/j3.mp4");
        pijama.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                pijamaUri=uri.toString();
            }
        });
        StorageReference bagaj=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/j/j4.mp4");
        bagaj.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                bagajUri=uri.toString();
            }
        });
        StorageReference oje=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/j/j5.mp4");
        oje.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                ojeUri=uri.toString();
            }
        });
        StorageReference ruj=storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/j/j6.mp4");
        ruj.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                rujUri=uri.toString();
            }
        });
        // get user
        firebaseAuth = FirebaseAuth.getInstance();
        rf = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/");
        sr = rf.child("User Letters").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail())).child("J Letter From User");
        srw = rf.child("User Words").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail())).child("J Words From User");

        jFilepath = Environment.getExternalStorageDirectory().getPath() + "/j.mp4";
        jipFile = Environment.getExternalStorageDirectory().getPath() + "/jip.mp4";
        pijamaFile = Environment.getExternalStorageDirectory().getPath() + "/pijama.mp4";
        bagajFile = Environment.getExternalStorageDirectory().getPath() + "/bagaj.mp4";
        ojeFile = Environment.getExternalStorageDirectory().getPath() + "/oje.mp4";
        rujFile = Environment.getExternalStorageDirectory().getPath() + "/ruj.mp4";


        j1 = findViewById(R.id.j1);

        AdRequest adRequest = new AdRequest.Builder().build();
        j1.loadAd(adRequest);

        j1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }
    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(jFilepath);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void stopRecording() {
        if (recorder != null) {
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
        }
    }
    private void startPlaying() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(jFilepath);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }


    private void startRecordingJip() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(jipFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingJip() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(jipFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingPijama() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(pijamaFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingPijama() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(pijamaFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingBagaj() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(bagajFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingBagaj() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(bagajFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingOje() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(ojeFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingOje() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(ojeFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingRuj() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(rujFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingRuj() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(rujFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }


    @Override
    public void onClick(View v) {
        if (v == bJHow) {
            Intent intent = new Intent(getApplicationContext(), JSpeakActivity.class);
            startActivity(intent);
        } else if (v == bJRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bJRecord.getText().toString())) {
                startRecording();
                bJRecord.setText(stop);
            } else {
                stopRecording();
                bJRecord.setText(record);
                startPlaying();
            }

        } else if (v == bJUp) {
            Uri uri = Uri.fromFile(new File(jFilepath));
            storageReference = sr.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bJipR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bJipR.getText().toString())) {
                startRecordingJip();
                bJipR.setText(stop);
            } else {
                stopRecording();
                bJipR.setText(record);
                startPlayingJip();
            }
        } else if (v == bJipU) {
            Uri uri = Uri.fromFile(new File(jipFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bPijamaR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bPijamaR.getText().toString())) {
                startRecordingPijama();
                bPijamaR.setText(stop);
            } else {
                stopRecording();
                bPijamaR.setText(record);
                startPlayingPijama();
            }
        } else if (v == bPijamaU) {
            Uri uri = Uri.fromFile(new File(pijamaFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bBagajR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bBagajR.getText().toString())) {
                startRecordingBagaj();
                bBagajR.setText(stop);
            } else {
                stopRecording();
                bBagajR.setText(record);
                startPlayingBagaj();
            }
        } else if (v == bBagajU) {
            Uri uri = Uri.fromFile(new File(bagajFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bOjeR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bOjeR.getText().toString())) {
                startRecordingOje();
                bOjeR.setText(stop);
            } else {
                stopRecording();
                bOjeR.setText(record);
                startPlayingOje();
            }
        } else if (v == bOjeU) {
            Uri uri = Uri.fromFile(new File(ojeFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bRujR) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bRujR.getText().toString())) {
                startRecordingRuj();
                bRujR.setText(stop);
            } else {
                stopRecording();
                bRujR.setText(record);
                startPlayingRuj();
            }
        } else if (v == bRujU) {
            Uri uri = Uri.fromFile(new File(rujFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        }
    }

    public void j_play(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(myUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ruj(View view) {

        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(rujUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void oje(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(ojeUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bagaj(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(bagajUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pijama(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(pijamaUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void jip(View view) {
        try {
            mediaPlayer=new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(jipUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
