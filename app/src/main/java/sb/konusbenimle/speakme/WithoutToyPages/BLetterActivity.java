package sb.konusbenimle.speakme.WithoutToyPages;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import sb.konusbenimle.speakme.VoiceExplanationPages.BSpeakActivity;


public class BLetterActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = "ex";
    Button bBHow, bBRecord, bBUp, bPaintRecord, bPaintUp, bTabakRecord, bTabakUp, bKnifeRecord, bKnifeUp, bSobaRecor, bSobaUp, bBabyRecord, bBabyUp;
    private MediaPlayer mediaPlayer;
    private MediaRecorder recorder;

    private String bFilepath, babyFile, tabakFile, knifeFile, sobaFile, paintFile;

    private Uri file;
    String myUri, uriBaby, uriKnife, uriSoba, uriTabak, uriPaint;
    FirebaseAuth firebaseAuth;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference rf, sr, storageReference, srw;
    private AdView b1, b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bletter);

        bBHow = findViewById(R.id.b_how);
        bBHow.setOnClickListener(this);
        bBRecord = findViewById(R.id.b_record);
        bBRecord.setOnClickListener(this);
        bBUp = findViewById(R.id.b_up);
        bBUp.setOnClickListener(this);

        bPaintRecord = findViewById(R.id.paint_record);
        bPaintRecord.setOnClickListener(this);
        bPaintUp = findViewById(R.id.paint_up);
        bPaintUp.setOnClickListener(this);
        bBabyRecord = findViewById(R.id.baby_record);
        bBabyRecord.setOnClickListener(this);
        bBabyUp = findViewById(R.id.baby_up);
        bBabyUp.setOnClickListener(this);
        bTabakRecord = findViewById(R.id.tabak_record);
        bTabakRecord.setOnClickListener(this);
        bTabakUp = findViewById(R.id.tabak_up);
        bTabakUp.setOnClickListener(this);
        bKnifeRecord = findViewById(R.id.knife_record);
        bKnifeRecord.setOnClickListener(this);
        bKnifeUp = findViewById(R.id.knife_up);
        bKnifeUp.setOnClickListener(this);
        bSobaRecor = findViewById(R.id.soba_record);
        bSobaRecor.setOnClickListener(this);
        bSobaUp = findViewById(R.id.soba_up);
        bSobaUp.setOnClickListener(this);


        // get sounds from firebase storage

        StorageReference storageRef = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/letters/b.mp4");
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                myUri = uri.toString();
            }
        });

        StorageReference storagBaby = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/b/b1.mp4");
        storagBaby.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                uriBaby = uri.toString();
            }
        });

        StorageReference storageKnife = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/b/b3.mp4");
        storageKnife.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                uriKnife = uri.toString();
            }
        });

        StorageReference storageSoba = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/b/b2.mp4");
        storageSoba.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                uriSoba = uri.toString();
            }
        });

        StorageReference storagetbak = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/b/b5.mp4");
        storagetbak.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                uriTabak = uri.toString();
            }
        });

        StorageReference storagepaint = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/words/b/b4.mp4");
        storagepaint.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                uriPaint = uri.toString();
            }
        });

        bFilepath = Environment.getExternalStorageDirectory().getPath() + "/b.mp4";
        babyFile = Environment.getExternalStorageDirectory().getPath() + "/bebek.mp4";
        tabakFile = Environment.getExternalStorageDirectory().getPath() + "/tabak.mp4";
        knifeFile = Environment.getExternalStorageDirectory().getPath() + "/bıcak.mp4";
        paintFile = Environment.getExternalStorageDirectory().getPath() + "/boya.mp4";
        sobaFile = Environment.getExternalStorageDirectory().getPath() + "/soba.mp4";

        firebaseAuth = FirebaseAuth.getInstance();
        rf = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/");
        sr = rf.child("User Letters").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail())).child("B Letter From User");
        srw = rf.child("User Words").child(Objects.requireNonNull(firebaseAuth.getCurrentUser().getEmail())).child("B Words From User");

        // ads

        b1 = findViewById(R.id.b1);

        AdRequest adRequest = new AdRequest.Builder().build();
        b1.loadAd(adRequest);

        b1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }
    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(bFilepath);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void stopRecording() {
        if (recorder != null) {
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
        }
    }
    private void startPlaying() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(bFilepath);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingBaby() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(babyFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingBaby() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(babyFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingKnife() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(knifeFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingKnife() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(knifeFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingSoba() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(sobaFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingSoba() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(sobaFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingPaint() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(paintFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingPaint() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(paintFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    private void startRecordingtabak() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(tabakFile);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }
    private void startPlayingSTabak() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setVolume(1.0f, 1.0f);
        try {
            mediaPlayer.setDataSource(tabakFile);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer arg0) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(View v) {
        if (v == bBHow) {
            Intent intent = new Intent(getApplicationContext(), BSpeakActivity.class);
            startActivity(intent);
        } else if (v == bBRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bBRecord.getText().toString())) {
                startRecording();
                bBRecord.setText(stop);
            } else {
                stopRecording();
                bBRecord.setText(record);
                startPlaying();
            }
        } else if (v == bBUp) {
            Uri uri = Uri.fromFile(new File(bFilepath));
            storageReference = sr.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bSobaRecor) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bSobaRecor.getText().toString())) {
                startRecordingSoba();
                bSobaRecor.setText(stop);
            } else {
                stopRecording();
                bSobaRecor.setText(record);
                startPlayingSoba();
            }
        } else if (v == bSobaUp) {
            Uri uri = Uri.fromFile(new File(sobaFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bKnifeRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bKnifeRecord.getText().toString())) {
                startRecordingKnife();
                bKnifeRecord.setText(stop);
            } else {
                stopRecording();
                bKnifeRecord.setText(record);
                startPlayingKnife();
            }
        } else if (v == bKnifeUp) {
            Uri uri = Uri.fromFile(new File(knifeFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bTabakRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bTabakRecord.getText().toString())) {
                startRecordingtabak();
                bTabakRecord.setText(stop);
            } else {
                stopRecording();
                bTabakRecord.setText(record);
                startPlayingSTabak();
            }
        } else if (v == bTabakUp) {
            Uri uri = Uri.fromFile(new File(tabakFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bBabyRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bBabyRecord.getText().toString())) {
                startRecordingBaby();
                bBabyRecord.setText(stop);
            } else {
                stopRecording();
                bBabyRecord.setText(record);
                startPlayingBaby();
            }
        } else if (v == bBabyUp) {
            Uri uri = Uri.fromFile(new File(babyFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else if (v == bPaintRecord) {
            String record = getResources().getString(R.string.record);
            String stop = getResources().getString(R.string.stop);
            if (record.equals(bPaintRecord.getText().toString())) {
                startRecordingPaint();
                bPaintRecord.setText(stop);
            } else {
                stopRecording();
                bPaintRecord.setText(record);
                startPlayingPaint();
            }
        } else if (v == bPaintUp) {
            Uri uri = Uri.fromFile(new File(paintFile));
            storageReference = srw.child(uri.getLastPathSegment());
            storageReference.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Ses gönderildi", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        }
    }

    public void b_play(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(myUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void baby(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(uriBaby);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void knife(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(uriKnife);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void paint(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(uriPaint);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void soba(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(uriSoba);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tabak(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(uriTabak);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}