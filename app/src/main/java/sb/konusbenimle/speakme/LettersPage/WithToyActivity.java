package sb.konusbenimle.speakme.LettersPage;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import sb.konusbenimle.speakme.WithToyPages.TBLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TCCLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TCLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TDLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TFLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TGLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.THLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TJLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TKLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TLLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TMLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TNLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TPLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TRLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TSLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TSSLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TTLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TVLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TYLetterActivity;
import sb.konusbenimle.speakme.WithToyPages.TZLetterActivity;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class WithToyActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_AUDIO_PERMISSION_CODE = 12349;
    ImageView iBLetT, iCLetT, iccLetT, iDLetT, iFLetT, iGLetT,
            iHLetT, iJLetT, iKLetT, iLLetT, iMLetT, iNLetT,
            iPLetT, iRLetT, iSLetT, issLetT, iTLetT, iVLEtT, iYLetT, iZLEtT;
    private AdView mAdView2, mAdView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_toy);

        requestPermissions();

        iBLetT = findViewById(R.id.b_let_t);
        iCLetT = findViewById(R.id.c_let_t);
        iccLetT = findViewById(R.id.ç_let_t);
        iDLetT = findViewById(R.id.d_let_t);
        iFLetT = findViewById(R.id.f_let_t);
        iGLetT = findViewById(R.id.g_let_t);
        iHLetT = findViewById(R.id.h_let_t);
        iJLetT = findViewById(R.id.j_let_t);
        iKLetT = findViewById(R.id.k_let_t);
        iLLetT = findViewById(R.id.l_let_t);
        iMLetT = findViewById(R.id.m_let_t);
        iNLetT = findViewById(R.id.n_let_t);
        iPLetT = findViewById(R.id.p_let_t);
        iRLetT = findViewById(R.id.r_let_t);
        iSLetT = findViewById(R.id.s_let_t);
        issLetT = findViewById(R.id.ş_let_t);
        iTLetT = findViewById(R.id.t_let_t);
        iVLEtT = findViewById(R.id.v_let_t);
        iYLetT = findViewById(R.id.y_let_t);
        iZLEtT = findViewById(R.id.z_let_t);

        iBLetT.setOnClickListener(this);
        iCLetT.setOnClickListener(this);
        iccLetT.setOnClickListener(this);
        iDLetT.setOnClickListener(this);
        iFLetT.setOnClickListener(this);
        iGLetT.setOnClickListener(this);
        iHLetT.setOnClickListener(this);
        iJLetT.setOnClickListener(this);
        iKLetT.setOnClickListener(this);
        iLLetT.setOnClickListener(this);
        iMLetT.setOnClickListener(this);
        iNLetT.setOnClickListener(this);
        iPLetT.setOnClickListener(this);
        iRLetT.setOnClickListener(this);
        iSLetT.setOnClickListener(this);
        issLetT.setOnClickListener(this);
        iTLetT.setOnClickListener(this);
        iVLEtT.setOnClickListener(this);
        iYLetT.setOnClickListener(this);
        iZLEtT.setOnClickListener(this);

        mAdView2 = findViewById(R.id.adView2);

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView2.loadAd(adRequest);

        mAdView2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }
    @Override
    public void onClick(View v) {

        if (v == iBLetT) {
            Intent intent = new Intent(getApplicationContext(), TBLetterActivity.class);
            startActivity(intent);
        } else if (v == iCLetT) {
            Intent intent = new Intent(getApplicationContext(), TCLetterActivity.class);
            startActivity(intent);
        } else if (v == iccLetT) {
            Intent intent = new Intent(getApplicationContext(), TCCLetterActivity.class);
            startActivity(intent);
        } else if (v == iDLetT) {
            Intent intent = new Intent(getApplicationContext(), TDLetterActivity.class);
            startActivity(intent);
        } else if (v == iFLetT) {
            Intent intent = new Intent(getApplicationContext(), TFLetterActivity.class);
            startActivity(intent);
        } else if (v == iGLetT) {
            Intent intent = new Intent(getApplicationContext(), TGLetterActivity.class);
            startActivity(intent);
        } else if (v == iHLetT) {
            Intent intent = new Intent(getApplicationContext(), THLetterActivity.class);
            startActivity(intent);
        } else if (v == iJLetT) {
            Intent intent = new Intent(getApplicationContext(), TJLetterActivity.class);
            startActivity(intent);
        } else if (v == iKLetT) {
            Intent intent = new Intent(getApplicationContext(), TKLetterActivity.class);
            startActivity(intent);
        } else if (v == iLLetT) {
            Intent intent = new Intent(getApplicationContext(), TLLetterActivity.class);
            startActivity(intent);
        } else if (v == iMLetT) {
            Intent intent = new Intent(getApplicationContext(), TMLetterActivity.class);
            startActivity(intent);
        } else if (v == iNLetT) {
            Intent intent = new Intent(getApplicationContext(), TNLetterActivity.class);
            startActivity(intent);
        } else if (v == iPLetT) {
            Intent intent = new Intent(getApplicationContext(), TPLetterActivity.class);
            startActivity(intent);
        } else if (v == iRLetT) {
            Intent intent = new Intent(getApplicationContext(), TRLetterActivity.class);
            startActivity(intent);
        } else if (v == iSLetT) {
            Intent intent = new Intent(getApplicationContext(), TSLetterActivity.class);
            startActivity(intent);
        } else if (v == issLetT) {
            Intent intent = new Intent(getApplicationContext(), TSSLetterActivity.class);
            startActivity(intent);
        } else if (v == iTLetT) {
            Intent intent = new Intent(getApplicationContext(), TTLetterActivity.class);
            startActivity(intent);
        }else if (v == iVLEtT) {
            Intent intent = new Intent(getApplicationContext(), TVLetterActivity.class);
            startActivity(intent);
        } else if (v == iYLetT) {
            Intent intent = new Intent(getApplicationContext(), TYLetterActivity.class);
            startActivity(intent);
        } else if (v == iZLEtT) {
            Intent intent = new Intent(getApplicationContext(), TZLetterActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_AUDIO_PERMISSION_CODE: // requestcode u eğer REQUEST_AUDIO_PERMISSION_CODE ile aynı olursa bu iznini verecektir
                if (grantResults.length > 0) {
                    //grantResult[0] birden fazla izin istenilmiş olabilir o yuzden parametreler olarak gelen grantresult değeri dizi olacaktır
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) { // ikisi de true ise..
                        //Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                        // PackageManager.PERMISSION_GRANTED : Bu int değeri iznin verilmiş olduğunu belirtir.
                    } else {
                        //Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                        // PackageManager.PERMISSION_DENIED : Bu int değeri iznin verilmemiş olduğunu belirtir.
                    }
                }
                break;
        }
    }

    public boolean checkPermissions() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        // izinler verilmişse sonuç olarak true değeri dönecektir.
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(WithToyActivity.this, new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, REQUEST_AUDIO_PERMISSION_CODE);

    }
}
