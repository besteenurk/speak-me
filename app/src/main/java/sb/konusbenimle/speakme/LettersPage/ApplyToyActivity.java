package sb.konusbenimle.speakme.LettersPage;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import sb.konusbenimle.speakme.Main2Activity;

public class ApplyToyActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView iToy1, iToy2, iToy3, iToy4, iToy5, iToy6, iToy7, iToy8;
    Button bGirl, bBoy, bShopDone;
    LinearLayout lToys;
    TextView tCount, tToySelected;
    EditText eName, eEmail, ePhone;
    private AdView mAdView4, mAdView5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_toy);

        iToy1 = findViewById(R.id.toy1);
        iToy1.setOnClickListener(this);
        iToy2 = findViewById(R.id.toy2);
        iToy2.setOnClickListener(this);
        iToy3 = findViewById(R.id.toy3);
        iToy3.setOnClickListener(this);
        iToy4 = findViewById(R.id.toy4);
        iToy4.setOnClickListener(this);

        bBoy = findViewById(R.id.boy);
        bBoy.setOnClickListener(this);
        bGirl = findViewById(R.id.girl);
        bGirl.setOnClickListener(this);
        bShopDone = findViewById(R.id.shop_done);
        bShopDone.setOnClickListener(this);

        lToys = findViewById(R.id.toys);
        lToys.setVisibility(View.GONE);

        tCount = findViewById(R.id.count);
        tCount.setVisibility(View.GONE);
        tToySelected = findViewById(R.id.toy_selected);
        tToySelected.setOnClickListener(this);

        eEmail = findViewById(R.id.email);
        eName = findViewById(R.id.name);
        ePhone = findViewById(R.id.phone);

        mAdView4 = findViewById(R.id.adView4);

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView4.loadAd(adRequest);

        mAdView4.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }
    public void ApplyToy() {
        String name = eName.getText().toString();
        String email = eEmail.getText().toString();
        String phone = ePhone.getText().toString();
        String toy = tToySelected.getText().toString();
        String count = tCount.getText().toString();

        if (name.isEmpty()) {
            eName.setError(getString(R.string.input_error_name));
            eName.requestFocus();
            return;
        }

        if (email.isEmpty()) {
            eEmail.setError(getString(R.string.input_error_email));
            eEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            eEmail.setError(getString(R.string.input_error_email_invalid));
            eEmail.requestFocus();
            return;
        }

        if (phone.isEmpty()) {
            ePhone.setError(getString(R.string.input_error_phone));
            ePhone.requestFocus();
            return;
        }

        if (phone.length() != 10) {
            ePhone.setError(getString(R.string.input_error_phone_invalid));
            ePhone.requestFocus();
            return;
        }


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Toy Orders").child(name);

        myRef.child("User name").setValue(name).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(getApplicationContext(), getString(R.string.registration_success), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                    finish();
                    startActivity(intent);
                }else {
                    eName.setError("İstek gönderilemedi");
                }
            }
        });
        myRef.child("User email").setValue(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    eEmail.setText("");
                }else {
                    eEmail.setError("İstek gönderilemedi");
                }
            }
        });
        myRef.child("User phone").setValue(phone).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    ePhone.setText("");
                }else {
                    ePhone.setError("İstek gönderilemedi");
                }
            }
        });
        myRef.child("Toy url").setValue(toy).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    tToySelected.setText("");
                }else {
                    tToySelected.setError("İstek gönderilemedi");
                }
            }
        });
        myRef.child("Total count").setValue(count).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    tToySelected.setText("");
                }else {
                    tToySelected.setError("İstek gönderilemedi");
                }
            }
        });


    }



    @Override
    public void onClick(View v) {
        if (v == bBoy) {
            lToys.setVisibility(View.VISIBLE);
            iToy1.setImageResource(R.drawable.t2);
            iToy2.setImageResource(R.drawable.t7);
            iToy3.setImageResource(R.drawable.t4);
            iToy4.setImageResource(R.drawable.t6);
        } else if (v == bGirl) {
            lToys.setVisibility(View.VISIBLE);
            iToy1.setImageResource(R.drawable.t1);
            iToy2.setImageResource(R.drawable.t7);
            iToy3.setImageResource(R.drawable.t5);
            iToy4.setImageResource(R.drawable.t3);
        } else if (v == iToy1) {
                tCount.setVisibility(View.VISIBLE);
                tCount.setText("Toplam ücret 200 TL");
                tToySelected.setText("https://www.ciceksepeti.com/65-cm-pembe-uyku-arkadasim-pelus-fil-kc614929?source=search&campaignid=1666779012&adgroupid=64222103277&keyword=&targetid=pla-600966504436&placement=&device=c&product_id=kc614929&gclid=CjwKCAjwibzsBRAMEiwA1pHZrtrQSsl7NERDDfCWq76ZdI9ktA_Lbx4xzpBcEypMykOS2Jqqhgv4ZBoC_VcQAvD_BwE");
        } else if (v == iToy2) {
                tCount.setVisibility(View.VISIBLE);
                tCount.setText("Toplam ücret 160 TL");
                tToySelected.setText("https://urun.gittigidiyor.com/oyuncak/sozzy-toys-uyku-arkadasim-pelus-fil-oyuncak-65-cm-sozzy-fil-5-renk-secenegi-ile-474910638");
        } else if (v == iToy3) {
                tCount.setVisibility(View.VISIBLE);
                tCount.setText("Toplam ücret 120 TL");
                tToySelected.setText("https://urun.gittigidiyor.com/oyuncak/pelus-tavsan-oyuncak-pelus-sevgiliye-hediye-bebek-cocuk-oyuncagi-oda-susu-uyku-arkadasim-d-466541512");
        } else if (v == iToy4) {
                tCount.setVisibility(View.VISIBLE);
                tCount.setText("Toplam ücret 110 TL");
                tToySelected.setText("https://urun.gittigidiyor.com/oyuncak/pelus-esek-30cm-sevimli-pelus-sevgiliye-hediye-bebek-cocuk-oyuncagi-oda-susu-uyku-arkadasim-h-395393271?_sgm_campaign=scn_7f87b180014b6000&_sgm_source=395393271&_sgm_action=click?rcp_action=rcpProductClicked");
        } else if (v == bShopDone) {
            ApplyToy();
        }

    }
}
