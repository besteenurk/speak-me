package sb.konusbenimle.speakme.LettersPage;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class EvaluateAudioActivity extends AppCompatActivity {

    private DatabaseReference dr;
    private StorageReference storageReference;
    private List<String> list = new ArrayList<>();
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser, fu;
    private ListView lv;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    String doktorUri, bebekUri, fareUri, timsahUri, uzumUri, horozUri, aslanUri, ahtapotUri, armutUri, tavaUri,zurafaUri, yaziUri;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluate_audio);


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        assert firebaseUser != null;
        final String b = firebaseUser.getEmail();



        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, list);


        StorageReference doktor = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/D Words From User/doktor.mp4");
        doktor.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                doktorUri = uri.toString();
            }
        });
        StorageReference bebek = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/B Words From User/bebek.mp4");
        bebek.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                bebekUri = uri.toString();
            }
        });
        StorageReference fare = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/F Words From User/fare.mp4");
        fare.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                fareUri = uri.toString();
            }
        });
        StorageReference timsah = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/M Words From User/timsah.mp4");
        fare.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                timsahUri = uri.toString();
            }
        });
        StorageReference uzum = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/M Words From User/uzum.mp4");
        uzum.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                uzumUri = uri.toString();
            }
        });
        StorageReference horoz = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/R Words From User/roroz.mp4");
        horoz.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                horozUri = uri.toString();
            }
        });
        StorageReference aslan = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/S Words From User/aslan.mp4");
        aslan.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                aslanUri = uri.toString();
            }
        });
        StorageReference ahtapot = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/T Words From User/ahtapott.mp4");
        ahtapot.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                ahtapotUri = uri.toString();
            }
        });
        StorageReference armut = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/T Words From User/armut.mp4");
        armut.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                armutUri = uri.toString();
            }
        });
        StorageReference tava = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/V Words From User/tava.mp4");
        tava.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                tavaUri = uri.toString();
            }
        });
        StorageReference yazi = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/Z Words From User/yazi.mp4");
        yazi.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                yaziUri = uri.toString();
            }
        });
        StorageReference zurafa = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words/kullanici@gmail.com/Z Words From User/zurafa.mp4");
        zurafa.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                zurafaUri = uri.toString();
            }
        });


    }

    public void doktor_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(doktorUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bebek_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(bebekUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void timsah_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(timsahUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uzum_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(uzumUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fare_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(fareUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ahtapot_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(ahtapotUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void horoz_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(horozUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void aslan_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(aslanUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void armut_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(armutUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void zurafa_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(zurafaUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void yazi_l(View view) {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(yaziUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
