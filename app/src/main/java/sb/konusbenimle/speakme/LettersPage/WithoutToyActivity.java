package sb.konusbenimle.speakme.LettersPage;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import sb.konusbenimle.speakme.WithoutToyPages.BLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.CCLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.CLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.DLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.FLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.GLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.HLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.JLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.KLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.LLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.MLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.NLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.PLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.RLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.SLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.SSLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.TLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.VLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.YLetterActivity;
import sb.konusbenimle.speakme.WithoutToyPages.ZLetterActivity;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class WithoutToyActivity extends AppCompatActivity implements View.OnClickListener {


    private static final int REQUEST_AUDIO_PERMISSION_CODE = 12349;
    ImageView iBLet, iCLet, iccLet, iDLet, iFLet, iGLet,
            iHLet, iJLet, iKLet, iLLet, iMLet, iNLet,
            iPLet, iRLet, iSLet, issLet, iTLet, iVLEt, iYLet, iZLEt;
    private AdView mAdView, mAdView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_without_toy);

        requestPermissions();

        iBLet = findViewById(R.id.b_let);
        iCLet = findViewById(R.id.c_let);
        iccLet = findViewById(R.id.ç_let);
        iDLet = findViewById(R.id.d_let);
        iFLet = findViewById(R.id.f_let);
        iGLet = findViewById(R.id.g_let);
        iHLet = findViewById(R.id.h_let);
        iJLet = findViewById(R.id.j_let);
        iKLet = findViewById(R.id.k_let);
        iLLet = findViewById(R.id.l_let);
        iMLet = findViewById(R.id.m_let);
        iNLet = findViewById(R.id.n_let);
        iPLet = findViewById(R.id.p_let);
        iRLet = findViewById(R.id.r_let);
        iSLet = findViewById(R.id.s_let);
        issLet = findViewById(R.id.ş_let);
        iTLet = findViewById(R.id.t_let);
        iVLEt = findViewById(R.id.v_let);
        iYLet = findViewById(R.id.y_let);
        iZLEt = findViewById(R.id.z_let);

        iBLet.setOnClickListener(this);
        iCLet.setOnClickListener(this);
        iccLet.setOnClickListener(this);
        iDLet.setOnClickListener(this);
        iFLet.setOnClickListener(this);
        iGLet.setOnClickListener(this);
        iHLet.setOnClickListener(this);
        iJLet.setOnClickListener(this);
        iKLet.setOnClickListener(this);
        iLLet.setOnClickListener(this);
        iMLet.setOnClickListener(this);
        iNLet.setOnClickListener(this);
        iPLet.setOnClickListener(this);
        iRLet.setOnClickListener(this);
        iSLet.setOnClickListener(this);
        issLet.setOnClickListener(this);
        iTLet.setOnClickListener(this);
        iVLEt.setOnClickListener(this);
        iYLet.setOnClickListener(this);
        iZLEt.setOnClickListener(this);


        mAdView = findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == iBLet) {
            Intent intent = new Intent(getApplicationContext(), BLetterActivity.class);
            startActivity(intent);
        } else if (v == iCLet) {
            Intent intent = new Intent(getApplicationContext(), CLetterActivity.class);
            startActivity(intent);
        } else if (v == iccLet) {
            Intent intent = new Intent(getApplicationContext(), CCLetterActivity.class);
            startActivity(intent);
        } else if (v == iDLet) {
            Intent intent = new Intent(getApplicationContext(), DLetterActivity.class);
            startActivity(intent);
        } else if (v == iFLet) {
            Intent intent = new Intent(getApplicationContext(), FLetterActivity.class);
            startActivity(intent);
        } else if (v == iGLet) {
            Intent intent = new Intent(getApplicationContext(), GLetterActivity.class);
            startActivity(intent);
        } else if (v == iHLet) {
            Intent intent = new Intent(getApplicationContext(), HLetterActivity.class);
            startActivity(intent);
        } else if (v == iJLet) {
            Intent intent = new Intent(getApplicationContext(), JLetterActivity.class);
            startActivity(intent);
        } else if (v == iKLet) {
            Intent intent = new Intent(getApplicationContext(), KLetterActivity.class);
            startActivity(intent);
        } else if (v == iLLet) {
            Intent intent = new Intent(getApplicationContext(), LLetterActivity.class);
            startActivity(intent);
        } else if (v == iMLet) {
            Intent intent = new Intent(getApplicationContext(), MLetterActivity.class);
            startActivity(intent);
        } else if (v == iNLet) {
            Intent intent = new Intent(getApplicationContext(), NLetterActivity.class);
            startActivity(intent);
        } else if (v == iPLet) {
            Intent intent = new Intent(getApplicationContext(), PLetterActivity.class);
            startActivity(intent);
        } else if (v == iRLet) {
            Intent intent = new Intent(getApplicationContext(), RLetterActivity.class);
            startActivity(intent);
        } else if (v == iSLet) {
            Intent intent = new Intent(getApplicationContext(), SLetterActivity.class);
            startActivity(intent);
        } else if (v == issLet) {
            Intent intent = new Intent(getApplicationContext(), SSLetterActivity.class);
            startActivity(intent);
        } else if (v == iTLet) {
            Intent intent = new Intent(getApplicationContext(), TLetterActivity.class);
            startActivity(intent);
        }else if (v == iVLEt) {
            Intent intent = new Intent(getApplicationContext(), VLetterActivity.class);
            startActivity(intent);
        } else if (v == iYLet) {
            Intent intent = new Intent(getApplicationContext(), YLetterActivity.class);
            startActivity(intent);
        } else if (v == iZLEt) {
            Intent intent = new Intent(getApplicationContext(), ZLetterActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_AUDIO_PERMISSION_CODE: // requestcode u eğer REQUEST_AUDIO_PERMISSION_CODE ile aynı olursa bu iznini verecektir
                if (grantResults.length > 0) {
                    //grantResult[0] birden fazla izin istenilmiş olabilir o yuzden parametreler olarak gelen grantresult değeri dizi olacaktır
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) { // ikisi de true ise..
                        //Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                        // PackageManager.PERMISSION_GRANTED : Bu int değeri iznin verilmiş olduğunu belirtir.
                    } else {
                        //Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                        // PackageManager.PERMISSION_DENIED : Bu int değeri iznin verilmemiş olduğunu belirtir.
                    }
                }
                break;
        }
    }

    public boolean checkPermissions() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        // izinler verilmişse sonuç olarak true değeri dönecektir.
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(WithoutToyActivity.this, new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, REQUEST_AUDIO_PERMISSION_CODE);

    }
}
