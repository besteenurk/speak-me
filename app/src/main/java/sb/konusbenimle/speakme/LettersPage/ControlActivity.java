package sb.konusbenimle.speakme.LettersPage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class ControlActivity extends AppCompatActivity implements View.OnClickListener {

    Button check;
    String num_user;
    TextView num;
    private AdView mAdView6, mAdView7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        check = findViewById(R.id.check);
        check.setOnClickListener(this);
        num = findViewById(R.id.check_num);
        num_user = "9911773355";

        mAdView6 = findViewById(R.id.adView6);

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView6.loadAd(adRequest);

        mAdView6.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == check) {
            if (num.getText().toString().equals(num_user)) {
                Toast.makeText(getApplicationContext(), "Kod doğrulandı", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), WithToyActivity.class);
                startActivity(intent);
            } else
                Toast.makeText(getApplicationContext(), "Kod hatası", Toast.LENGTH_LONG).show();
        }
    }
}
