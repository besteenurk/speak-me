package sb.konusbenimle.speakme.LettersPage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class UserAudioActivity extends AppCompatActivity {

    FirebaseStorage storage = FirebaseStorage.getInstance();
    TextView usr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_audio);

        usr = findViewById(R.id.usr);
        usr.setText("Kullanıcı Test");
        usr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EvaluateAudioActivity.class);
                startActivity(intent);
            }
        });


        StorageReference storageRef = storage.getReferenceFromUrl("gs://speak-me-dedb0.appspot.com/User Words");
    }
}
