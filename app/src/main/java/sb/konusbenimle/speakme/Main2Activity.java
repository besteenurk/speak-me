package sb.konusbenimle.speakme;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.speakme.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import sb.konusbenimle.speakme.Auth.UserProfileActivity;
import sb.konusbenimle.speakme.InformationPages.ArtInfActivity;
import sb.konusbenimle.speakme.LettersPage.ApplyToyActivity;
import sb.konusbenimle.speakme.LettersPage.ControlActivity;
import sb.konusbenimle.speakme.LettersPage.UserAudioActivity;
import sb.konusbenimle.speakme.LettersPage.WithoutToyActivity;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    TextView textView, dene;
    Button bArt, bWithToy, bEvaluateAudio;
    private AdView a1, a2;
    private DatabaseReference databaseReference, dr;
    private String user;
    private List<String> list = new ArrayList<>();
    private ListView listView;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser, fu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        bEvaluateAudio = findViewById(R.id.evaluate_audio);
        bEvaluateAudio.setOnClickListener(this);
        bEvaluateAudio.setVisibility(View.GONE);

        listView = findViewById(R.id.lw);
        dene = findViewById(R.id.dene);
        //dene.setVisibility(View.GONE);

        bArt = findViewById(R.id.artikülasyon);
        // bWithToy = findViewById(R.id.withtoy);

        bArt.setOnClickListener(this);
        // bWithToy.setOnClickListener(this);

        a1 = findViewById(R.id.a1);

        AdRequest adRequest = new AdRequest.Builder().build();
        a1.loadAd(adRequest);
        a1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        assert firebaseUser != null;
        final String b = firebaseUser.getEmail();

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, list);

        databaseReference = FirebaseDatabase.getInstance().getReference("Therapist email");

        databaseReference.child("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    String a = (String) ds.getValue();
                    list.add(a);
                   // listView.setAdapter(arrayAdapter);
                    for (int i = 0; i < list.size(); i++) {
                        if (list.contains(b)) {
                           // dene.setVisibility(View.VISIBLE);
                           // dene.setText("beste");
                            bEvaluateAudio.setVisibility(View.VISIBLE);
                        } else {
                            //Toast.makeText(Main2Activity.this, "bulunamadı.", Toast.LENGTH_LONG).show();
                        }

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action

        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(getApplicationContext(), UserProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_withoutToy) {
            Intent intent = new Intent(getApplicationContext(), WithoutToyActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_withToy) {
            Intent intent = new Intent(getApplicationContext(), ControlActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_applyToy) {
            Intent intent = new Intent(getApplicationContext(), ApplyToyActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onClick(View v) {

        if (v == bArt) {
            Intent intent = new Intent(getApplicationContext(), ArtInfActivity.class);
            startActivity(intent);
        } else if (v == bEvaluateAudio) {
            Intent intent = new Intent(getApplicationContext(), UserAudioActivity.class);
            startActivity(intent);
        }
    }
}
