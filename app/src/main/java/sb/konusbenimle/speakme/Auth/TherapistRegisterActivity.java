package sb.konusbenimle.speakme.Auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.speakme.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import sb.konusbenimle.speakme.Main2Activity;

public class TherapistRegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextNameTh, editTextEmailTh, editTextPasswordTh, editTextPhoneTh, editTextCompanyTh, editTextYearTh;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapist);

        editTextNameTh = findViewById(R.id.edit_text_name_th);
        editTextEmailTh = findViewById(R.id.edit_text_email_th);
        editTextPasswordTh = findViewById(R.id.edit_text_password_th);
        editTextPhoneTh = findViewById(R.id.edit_text_phone_th);
        editTextCompanyTh = findViewById(R.id.edit_text_company_th);
        editTextYearTh = findViewById(R.id.edit_text_year_th);
        //progressBar = findViewById(R.id.progressbar);
        //progressBar.setVisibility(View.GONE);

        mAuth = FirebaseAuth.getInstance();

        findViewById(R.id.button_register).setOnClickListener(this);


    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mAuth.getCurrentUser() != null) {
            //handle the already login user
        }
    }

    private void registerUser() {
        final String name = editTextNameTh.getText().toString().trim();
        final String email = editTextEmailTh.getText().toString().trim();
        String password = editTextPasswordTh.getText().toString().trim();
        final String phone = editTextPhoneTh.getText().toString().trim();
        final String company = editTextCompanyTh.getText().toString().trim();
        final String year = editTextYearTh.getText().toString().trim();

        if (name.isEmpty()) {
            editTextNameTh.setError(getString(R.string.input_error_name));
            editTextNameTh.requestFocus();
            return;
        }

        if (email.isEmpty()) {
            editTextEmailTh.setError(getString(R.string.input_error_email));
            editTextEmailTh.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmailTh.setError(getString(R.string.input_error_email_invalid));
            editTextEmailTh.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            editTextPasswordTh.setError(getString(R.string.input_error_password));
            editTextPasswordTh.requestFocus();
            return;
        }

        if (password.length() < 6) {
            editTextPasswordTh.setError(getString(R.string.input_error_password_length));
            editTextPasswordTh.requestFocus();
            return;
        }

        if (phone.isEmpty()) {
            editTextPhoneTh.setError(getString(R.string.input_error_phone));
            editTextPhoneTh.requestFocus();
            return;
        }

        if (phone.length() != 10) {
            editTextPhoneTh.setError(getString(R.string.input_error_phone_invalid));
            editTextPhoneTh.requestFocus();
            return;
        }


       // progressBar.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Therapist user = new Therapist(
                                    name,
                                    email,
                                    phone,
                                    company,
                                    year
                            );
                            FirebaseDatabase.getInstance().getReference("Therapist")
                                    //.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .child("user").child(name)
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                   // progressBar.setVisibility(View.GONE);
                                    if (task.isSuccessful()) {
                                        Toast.makeText(TherapistRegisterActivity.this, getString(R.string.registration_success), Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                                        finish();
                                        startActivity(intent);
                                    } else {
                                        //display a failure message
                                    }
                                }
                            });
                            FirebaseDatabase.getInstance().getReference("Therapist email").child("name").child(name)
                                    .setValue(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                   // progressBar.setVisibility(View.GONE);
                                    if (task.isSuccessful()) {
                                        Toast.makeText(TherapistRegisterActivity.this, getString(R.string.registration_success), Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                                        finish();
                                        startActivity(intent);
                                    } else {
                                        //display a failure message
                                    }
                                }
                            });


                        } else {
                            Toast.makeText(TherapistRegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            finish();
                            startActivity(getIntent());
                        }
                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_register:
                registerUser();
                break;
        }
    }
}