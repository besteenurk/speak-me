package sb.konusbenimle.speakme.Auth;

public class Therapist {
    public String name, email, phone, company, year;

    public Therapist(){

    }

    public Therapist(String name, String email, String phone, String company, String year ) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.company = company;
        this.year = year;
    }
}